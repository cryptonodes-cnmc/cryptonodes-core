► STEP 1 : CLONE INSTALLATION SCRIPT FROM GITLAB

> **UBUNTU 16 :** `wget https://gitlab.com/cryptonodes-cnmc/cryptonodes-core/-/blob/main/mn-installer/install-u16.sh`<br />
> **UBUNTU 18 :** `wget https://gitlab.com/cryptonodes-cnmc/cryptonodes-core/-/blob/main/mn-installer/install-u18.sh`


► STEP 2 : GIVE READ AND WRITE PERMISSION TO DOWNLOADED MN SCRIPT FILE

> **UBUNTU 16 :** `chmod 755 install-u16.sh`<br />
> **UBUNTU 18 :** `chmod 755 install-u18.sh`<br />


► STEP 4 : RUNNING AUTO INSTALL MASTERNODE SCRYPT

> **UBUNTU 16 :** `bash install-u16.sh`<br />
> **UBUNTU 18 :** `bash install-u18.sh`<br />


Now wait for some time for configuration and installation to get complete. 

► STEP 5 : ENTER YOUR MASTERNODE PRIVATE KEY GENERATED FROM LOCAL WALLET

THATS IT , YOUR CRYPTONODES MASTERNODE IS NOW RUNNING ON VPS. SIMPLY CONFIGURE YOUR LOCAL WALLET FOR MASTERNODE AND START.<br />

Desktop wallet setup After the MN is up and running, you need to configure the desktop wallet accordingly. Here are the steps for Windows Wallet<br />

Open the CRYPTONODES Wallet.<br />
> Go to RECEIVE and create a New Address: MN1<br />


> Send REQUIRED COLLATERAL to MN1.<br />
Wait for confirmations.<br />

► STEP 6 : Go to Tools -> "Debug console - Console"<br />
Type the following command: masternode outputs<br />

Go to ** > Tools -> "Open Masternode Configuration File" <br />
> • Add the following entry: <br />
> • Alias Address Privkey TxHash Output_index <br />
> • Alias: MN1 <br />
> • Address: VPS_IP:PORT <br />
> • Privkey: Masternode Private Key <br />
> • TxHash: First value from Step 6 <br />
> • Output index: Second value from Step 6 It can be 0 or 1<br />
> SAVE and exit the Wallet.<br />


Open CRYPTONODES Wallet, go to Masternode Tab. If you tab is not shown, <br />
please enable it from: Settings - Options - Wallet - Show Masternodes Tab<br />
Click Update status to see your node. If it is not shown, close the wallet and start it again.<br />

Click Start All or Start Alias<br />
If then also not starting then <br /> click on >tools>debug console> <br />

Type: 
> startmasternode alias false "name of your MN alias"
